<?php

/**
 * Reindex the panel contents in the hansel_panels_node table.
 * 
 * @param type $nid 
 */
function _hansel_panels_index_panel_contents($nid) {
  $child_nids = array();
  $sql = 'SELECT pp.configuration
  FROM {panels_node} pn
  JOIN {panels_pane} pp ON pn.did = pp.did
  WHERE pn.nid = %d';
  $res = db_query($sql, $nid);
  while ($pane = db_fetch_object($res)) {
    $pane->configuration = unserialize($pane->configuration);
    if (!empty($pane->configuration['nid'])) {
      $child_nids[] = $pane->configuration['nid'];
    }
  }
  
  $sql = 'DELETE FROM {hansel_panels_node} WHERE parent_nid = %d';
  db_query($sql, $nid);
  
  $sql = 'INSERT INTO {hansel_panels_node} (nid, parent_nid) VALUES (%d, %d)';
  foreach ($child_nids as $child_nid) {
    db_query($sql, $child_nid, $nid);
  }
}
